#!/bin/bash
# Prerequisites:
# Make sure all secrets are stored in files do not have a trailing
# newline.
# This can be achieved by echo -n "password" > password.txt

kubectl create secret generic user-profile-mysql-pass --from-file=mysql-password.txt --from-file=mysql-new-user-password.txt
kubectl create secret generic user-profile-aws-secrets --from-file=aws-access-key-id.txt --from-file=aws-secret-access-key.txt 

