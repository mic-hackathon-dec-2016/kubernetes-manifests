#!/bin/bash

# Make sure all secrets are stored in files do not have a trailing newline.
# This can be achieved by echo -n "password" > password.txt

kubectl create secret generic feed-reader-aws-secrets --from-file=aws-access-key-id.txt --from-file=aws-secret-access-key.txt 
kubectl create secret generic feed-reader-links-hack --from-file=links.txt

