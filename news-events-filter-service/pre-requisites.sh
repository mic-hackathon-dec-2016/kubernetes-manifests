#!/bin/bash -x
# Prerequisites:
# Make sure all secrets are stored in files do not have a trailing
# newline.
# This can be achieved by echo -n "password" > password.txt

# MySQL root password, and new user password
kubectl create secret generic news-events-filter-mysql-pass --from-file=mysql-password.txt --from-file=mysql-new-user-password.txt

# Naman AWS creds to access User Profile Subscriptions Queue
cd user-profile-secrets
kubectl create secret generic user-profile-fetcher-aws-secrets --from-file=aws-access-key-id.txt --from-file=aws-secret-access-key.txt
cd ..

# Debayan AWS creds to access News Events Subscriptions Queue
cd news-events-secrets
kubectl create secret generic news-fetcher-aws-secrets --from-file=aws-access-key-id.txt --from-file=aws-secret-access-key.txt
cd ..

