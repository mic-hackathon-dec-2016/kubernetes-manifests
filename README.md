# Description
This project hosts all k8s manifests for the microservices used in the dec 2016
hackathon

# Project Description

Personalized News Alert Service
    - An app which collects the news interest area of a registered user
and provides news feeds from various news resources from the web.

## Architecture

- Architecture: ![](./images/architecture.png) 

## Microservices
- User Profile Service: Allows a user to request a new
subscription based on interests and specify a desired frequency for
notifications. New Subscriptions are sent to "SubscriptionUpdates"
Queue.
- News feed reader Service: Scans for updates in news sources and sends
news events to a "News Events" Queue
- Events Filter Service: 
   Updates a local databased based on events from
Subscription Updates events. 
  (ScheduledJob) Receives events from News Events Queue, and 
figures out new news events to be sent to a user
  (ScheduledJob) Based on user's desired notification frequency, sends
  notifications on new news events to a User.

# Steps

1. Setup kubernetes cluster either in AWS using instructions at
http://kubernetes.io/docs/getting-started-guides/aws/ or elsewhere. The
K8S cluster must have batch/v2alpha1 API enabled as described in
http://kubernetes.io/docs/user-guide/cron-jobs/#prerequisites for the
execution of Scheduled Jobs. The following steps assume kubectl is 
configured to point to a valid K8S installation.
2. Each microservice can be deployed independently and in any order. The
K8S manifests for the microservices are hosted in this project.
    a. Go to a folder for a microservice. Say "news-feed-reader".
    b. If there are any pre-requisites needed (creation of K8S secrets
    to host AWS credentials, MySQL passwords etc), a pre-requisistes.sh
    bash script is available. Execute that script.  Modify the
    credentials to suit your installation prior to the execution of the
    script.
    c. There may be more than one Kubernetes manifest in a directory.
    Deploy those artifacts by executing
    $ kubectl create -f <service|scheduledjob>.yaml


